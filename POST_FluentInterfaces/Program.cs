﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POST_FluentInterfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            Post.Novo()
                .Titulo("Interfaces Fluentes")
                .Categoria("DSL")
                .Descricao("Novo post sobre Interfaces Fluentes")
                .Salvar();
        }
    }
}