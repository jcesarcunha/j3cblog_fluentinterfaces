﻿namespace POST_FluentInterfaces
{
    public class Post : IPost, IPostTitulo, IPostCategoria
    {
        private string titulo;
        private string categoria;
        private string descricao;
        private Post() { }

        public static IPostTitulo Novo()
        {
            return new Post();
        }

        public IPostCategoria Titulo(string titulo)
        {
            this.titulo = titulo;
            return this;
        }

        public IPost Categoria(string categoria)
        {
            this.categoria = categoria;
            return this;
        }

        public IPost Descricao(string descricao)
        {
            this.descricao = descricao;
            return this;
        }

        public void Salvar()
        {
            // Persiste o Post criado
        }
    }
}