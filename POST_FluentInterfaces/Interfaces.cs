﻿namespace POST_FluentInterfaces
{
    public interface IPost
    {
        IPost Descricao(string descricao);
        void Salvar();
    }

    public interface IPostTitulo
    {
        IPostCategoria Titulo(string titulo);
    }

    public interface IPostCategoria
    {
        IPost Categoria(string categoria);
    }
}